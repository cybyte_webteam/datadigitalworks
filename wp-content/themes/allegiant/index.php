<?php get_header(); ?>

<?php get_template_part('template-parts/element', 'page-header'); ?>

<?php if(cpotheme_show_posts()): ?>
<div id="case-studies" class="main">
	<div class="container">	
	<div id="case-studies-heading" class="case-studies-heading section-heading">Case Studies </div>
		<section id="content" class="content" style="width:auto !important">
			
			<?php do_action('cpotheme_before_content'); ?>
			<?php
				
				$post_start_desc =
			array('CyByte’s custom designed solution helped achieve near real-time data integration and saved 40% in licensing costs ',
				 'CyByte’s real-time data integration solution reduces turnaround time by 30%',
				'CyByte reduces patient re-admission to hospital by 30% by improving prescription compliance',
				'CyByte helps B2B insurance company save 40% in licensing costs', 
				'CyByte builds a market-ready, crowdfunding search engine to match potential investors with start-up',
				'CyByte helps to reduce cost per lead by 30% and increases lead pipelines by 45% via channel optimization');
				
				
				$i =0; if(have_posts())  while(have_posts()):  if($i%2 == 0 ) { echo '<div class="row">'; }   the_post(); ?>
			 <div class="column column-narrow col2">
			 <article <?php post_class(); ?> id="post-<?php the_ID(); ?>"> 
	<div class="post-image">
		<?php cpotheme_postpage_image(); ?>		
	</div>	
	<div class="post-body">
		<?php cpotheme_postpage_title(); ?>
		<!-- <div class="post-byline">
			<?php cpotheme_postpage_date(); ?>
			<?php cpotheme_postpage_author(); ?>
			<?php cpotheme_postpage_categories(); ?>
			<?php cpotheme_edit(); ?>
		</div> -->
		 
		<div class="post-content">			
			<div id="blog-post-less-<?php the_ID() ?>">
			<?php echo $post_start_desc[$i]; ?>
			<br><button class="post-readmore button" onclick="show_hide( <?php the_ID() ?> , true)"> Read More</button>
			</div>
			<div id="blog-post-more-<?php the_ID() ?>" style="display:none">
			<?php cpotheme_postpage_single_custom_content(); ?>
			<button class="post-readmore button" onclick="show_hide( <?php the_ID() ?> , false )" > Read Less</button>
			</div>
		</div>
		<!-- <?php cpotheme_postpage_comments(false, '%s'); ?>
		<?php if(is_singular('post')) cpotheme_postpage_tags(false, '', '', ''); ?>
		<?php cpotheme_postpage_readmore('button'); ?> -->
		<div class="clear"></div>
	</div>
</article>
			</div>
			<?php if($i%2 !=0 ) { echo '</div>'; } $i++; endwhile; ?>
			<?php cpotheme_numbered_pagination(); ?>
			<?php do_action('cpotheme_after_content'); ?>
			 
		</section>
		<?php get_sidebar(); ?>
		<div class="clear"></div>
	</div>
</div>
<?php endif; ?>


<?php get_footer(); ?>