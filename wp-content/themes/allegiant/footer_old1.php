<?php
$hostUrl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<link href="<?php echo $hostUrl;?>assets/css/style.css" rel="stylesheet" />
<link href="<?php echo $hostUrl;?>assets/css/custom-style.css" rel="stylesheet" />
 <link rel="stylesheet" type="text/css" href="<?php echo $hostUrl;?>assets/css/font-awesome.css"/>


<link href="<?php echo $hostUrl;?>assets/css/bootstrap.min.css" rel="stylesheet" />

<footer>
  <div class="container">
    <div class="address-way">
     <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
          <div class="inner-address"><span><img src="<?php echo $hostUrl;?>assets/images/address-iocn.png"></span>
            <p>52 Pine Creek Rd. Ste. 120, Wexford, PA 15090</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-3">
          <div class="inner-mail"><span><img src="<?php echo $hostUrl;?>assets/images/mail-icon.png"></span>
            <p><b><a title="info@cybyte.com" target="_top" href="mailto:info@cybyte.com?Subject=Hello%20Cybytetech">info@cybyte.com</a></b></p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-3">
          <div class="inner-phone"><span><img src="<?php echo $hostUrl;?>assets/images/phone-icon.png"></span>
            <p><b><a href="tel:412%.%837.%9299" title="(412) 837-9299">(412) 837-9299</a></b></p>
          </div>
        </div>
      </div>
      <div class="footer-inner-2">
        <div class="footer-nav col-md-7 col-xs-12 col-sm-7" style="padding:0px !important">
          <ul>
            <li><a href="<?php echo $hostUrl;?>#who-we-are" title="Who We Are"><span class="glyphicon glyphicon-menu-right"></span>Who We Are</a></li>
            <li><a href="<?php echo $hostUrl;?>#what-we-do" title="What We Do"><span class="glyphicon glyphicon-menu-right"></span>What We Do</a></li>
            <li><a href="<?php echo $hostUrl;?>#talend-services" title="Talend Services"><span class="glyphicon glyphicon-menu-right"></span>Talend Services</a></li>
          </ul>
          <ul>
            <li><a href="<?php echo $hostUrl;?>#case-studies" title="Case studies"><span class="glyphicon glyphicon-menu-right"></span>Case studies</a></li>
            <li><a href="<?php echo $hostUrl;?>#contact-us" title="Contact Us"><span class="glyphicon glyphicon-menu-right"></span>Contact Us</a></li>
		  </ul>
        </div>
        <div class="col-md-5 social-outer col-sm-5">
          <div class="social-icons">
            <p style="color:#fff !important">Follow Us</p>
            <ul>
              <li class="fb"> <a target="_blank" href="https://www.facebook.com/cybyte" title="Facebook"><i class="fa fa-facebook"></i></a> </li>
              <li class="tw"> <a target="_blank" href="https://twitter.com/CybyteInc" title="Twitter"><i class="fa fa-twitter"></i></a></li>
              <li class="li"> <a target="_blank" href="https://www.linkedin.com/company/cybyte-inc" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              <li class="yt"> <a target="_blank" href="https://www.youtube.com/channel/UCqqBws6YqjN3A5Glt1MP2NQ" title="You Tube"><i class="fa fa-youtube-square"></i></a></li>
              <li class="gp"> <a target="_blank" href="https://plus.google.com/101622605310219491200/about" title="Google-Plus"><i class="fa fa-google-plus"></i></a></li>
              <li class="pn"> <a target="_blank" href="https://www.pinterest.com/cybyte" title="Pinterest"><i class="fa fa-pinterest-p"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-6 text-left col-xs-6  copyright-bt">
            <p style="margin:0;" id="copyR">© 2017 CyByte</p>
          </div>
          <div class="col-md-6 text-right col-xs-6 sitemap-bt"> 
          <!--<a href="http://cybyte.com/sitemap" title="Sitemap">Sitemap</a> <a onclick="privacyFunc();" href="javascript:void(0);" title="Privacy">Privacy</a>--> </div>
        </div>
      </div>
    </div>
  </div>
</footer>


<!-- =============================================  -->

</body>
</html>
<script>
function show_hide( id , display_more ){
	if(display_more){
		jQuery('#blog-post-less-'+id).hide(); //Adds 'a', removes 'b'
		jQuery('#blog-post-more-'+id).show(); 
	}	else {
		jQuery('#blog-post-less-'+id).show(); //Adds 'a', removes 'b'
		jQuery('#blog-post-more-'+id).hide(); 
	}

}
</script>

