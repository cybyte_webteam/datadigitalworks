<?php do_action('cpotheme_after_main'); ?>


<footer id="footer" class="footer secondary-color-bg dark">
    <div class="container">
        <div class="row addr">
            <div class="column col3">
                <span class="fa fa-map-marker"></span>
                52 Pine Creek Rd. Ste. 120, Wexford, PA 15090
            </div>
             <div class="column col3">
                 <span class="fa fa-envelope"></span>
                 <a href="mailto:info@cybyte.com" title="info@cybyte.com">info@cybyte.com</a>
             </div>
             <div class="column col3">
                 <span class="fa fa-phone"></span>
					<a href="tel:412%.%837.%9299" title="(412) 837-9299">(412) 837-9299</a>
             </div>
            </div>

        <div class="row" >
            <div class="column column-narrow col2" id="footer-nav">
                <div class="row">
                    <div class="column col3">
                        <ul class="parent-ul-footer">
                            <li>
                                <a  href="<?php echo home_url().'/#who-we-are'; ?>"><span class="fa fa-angle-right"></span>  Who We Are</a>
                            </li>
                            <li>
                                <a  href="<?php echo home_url().'/#what-we-do'; ?>"> <span class="fa fa-angle-right"></span> What We Do</a>
                            </li>
                            <li>
                                <a  href="<?php  echo home_url().'/#talend-services'; ?>"><span class="fa fa-angle-right"></span> Talend Services</a>
                            </li>
                        </ul>
                    </div>

                    <div class="column col3">
                        <ul class="parent-ul-footer">


                            <li>
                                <a  href="<?php echo home_url().'/#case-studies'; ?>"><span class="fa fa-angle-right"></span> Case studies</a>
                            </li>
                            <li>
                                <a href="<?php  echo home_url().'/#contact-us'; ?>"><span class="fa fa-angle-right"></span> Contact Us</a>
                            </li>

                        </ul>

                     </div>
                    </div>




            </div>
            <div class="column column-narrow col2">
                <div class="social-icons">

                    <ul>

                        <li class="fb"> <a href="https://www.facebook.com/cybyte" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a> </li>
                        <li class="tw"> <a href="https://twitter.com/CybyteInc" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li class="li"> <a href="https://www.linkedin.com/company/cybyte-inc" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li class="yt"> <a href="https://www.youtube.com/channel/UCqqBws6YqjN3A5Glt1MP2NQ" target="_blank" title="You Tube"><i class="fa fa-youtube-square"></i></a></li>
                        <li class="gp"> <a href="https://plus.google.com/101622605310219491200/about" target="_blank" title="Google-Plus"><i class="fa fa-google-plus"></i></a></li>
                        <li class="pn"> <a href="https://www.pinterest.com/cybyte" target="_blank" title="Pinterest"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="footer-bottom row">
            <div class="column column-narrow col1">
                Copyright © 2017 DataDigitalWorks, All Rights Reserved
            </div>
        </div>
    </div>
</footer>
<?php do_action('cpotheme_after_footer'); ?>

<div class="clear"></div>
</div><!-- wrapper -->
<?php do_action('cpotheme_after_wrapper'); ?>
</div><!-- outer -->
<?php wp_footer(); ?>
</body>

<script src="https://use.fontawesome.com/91e2b7684c.js"></script>

<script>
    function show_hide(id, display_more) {
        if (display_more) {
            jQuery('#blog-post-less-' + id).hide(); //Adds 'a', removes 'b'
            jQuery('#blog-post-more-' + id).show();

        } else {
            jQuery('#blog-post-less-' + id).show(); //Adds 'a', removes 'b'
            jQuery('#blog-post-more-' + id).hide();

        }

        var divPosition = jQuery('#post-'+id).offset();
        jQuery('html, body').animate({scrollTop: divPosition.top - jQuery('header').height()}, "fast");

    }

    jQuery(document).ready(function ($) {
        jQuery(document).on('load', function () {
            if (location.hash.length != 0) {

                var divPosition = $(location.hash).offset();
                $('html, body').animate({scrollTop: divPosition.top - $('header').height()}, "fast");
            }
        });
jQuery('.menu-link').on('click', function(){
jQuery('body').removeClass('menu-mobile-active');
		
});

    });
</script>

<style>

#footer-nav { text-align: center}
/*#footer-nav .parent-ul-footer{*/
    /*float: left !important;*/
/*}*/
#footer-nav .parent-ul-footer li{
    list-style: outside none none;
    margin: 5px 10px;
    display: block;
}
#footer-nav .parent-ul-footer{
    text-align: left;
}

#contact-us .fa, #footer .addr .fa{
    color: orange;
}
.address-custom a{
    color:white;
}

#footer .addr{
    margin-bottom:20px;
}
.social-icons > ul > li {
    display: inline-block;
    float: left;
    list-style: outside none none;
    margin-right: 5px;
}
.footer-bottom {
    border-top: 1px solid #3b3a3a;
    margin-top: 18px;
    padding: 8px 0;
}

.social-icons {
    float: right;

    padding-top: 3px
}
.social-icons>p {
    float: left;
    font-size: 20px;
    line-height: 27px;
    padding-right: 25px;
    padding-top: 5px
}
.social-icons>ul>li {
    list-style: none;
    display: inline-block;
    margin: 0 3px;
    float: left
}
.social-icons>ul>li>a {
    background: #fff;
    border-radius: 50%;
    float: left;
    font-size: 14px;
    height: 32px;
    text-align: center;
    width: 32px;
    color: #000;
    line-height:32px;

}
.social-icons>ul>li:nth-child(1) a {
    background: #3e5b98;
    color: #FFF;

}
.social-icons>ul>li:nth-child(2) a {
    background: #4da7de;
    color: #FFF;

}
.social-icons>ul>li:nth-child(3) a {
    background: #3371b7;
    color: #FFF;

}
.social-icons>ul>li:nth-child(4) a {
    background: #e02a20;
    color: #FFF;

}
.social-icons>ul>li:nth-child(5) a {
    background: #d93e2d;
    color: #FFF;

}
.social-icons>ul>li:nth-child(6) a {
    background: #c92619;
    color: #FFF;

}
.social-icons>ul>li.fb:hover a, .social-icons>ul>li.gp:hover a, .social-icons>ul>li.li:hover a, .social-icons>ul>li.pn:hover a, .social-icons>ul>li.tw:hover a, .social-icons>ul>li.yt:hover a {
    background: #eb6101;
    color: #FFF
}

.post .post-body {
    min-height: 159px;
}
#contact-us{
    background-image: url('<?php echo get_template_directory_uri().'/images/contactus.jpg' ?>');
}
#what-we-do{
    background-image: url('<?php echo get_template_directory_uri().'/images/integrate.jpg' ?>');
}

.menu-main li a {
    color: #515151 !important;
}
</style>
</html>
