<?php $query = new WP_Query('post_type=cpo_team&order=ASC&orderby=menu_order&meta_key=team_featured&meta_value=1&posts_per_page=-1'); ?>
<?php if($query->posts): $feature_count = 0; ?>
<div id="contact-us" class="team">
	<div class="container">
		<?php cpotheme_block('home_team', 'team-heading section-heading dark'); ?>
		
		<div class="row">
				<div class="column column-narrow col2">
					  <img src="<?php echo get_template_directory_uri(); ?>/images/contact-us.png" alt="contact us"/>
				</div>
				<div class="column column-narrow col2 address-custom">
					 <ul>
						<li>
							<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/address-icon.png" alt="address-icon">  -->
							  <span class="fa fa-map-marker"></span> 52 Pine Creek Rd. Ste. 120,
							Wexford, PA 15090 
							<br><br>
						</li>
						<li>
						 <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png" alt="phone-icon">  -->
							 <span class="fa fa-phone"></span> 
								<a href="tel:412%.%837.%9299" title="(412) 837-9299">(412) 837-9299</a></li>
						<li>
						  <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/mail-icon.png" alt="mail-icon">  -->
							 <span class="fa fa-envelope"></span> 
							<a href="mailto:info@cybyte.com" title="info@cybyte.com">info@cybyte.com</a></li>
						<li>
						<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/website-icon.png" alt="website-icon">   -->
						<span class="fa fa-external-link"></span> 
						<a href="http://www.cybyte.com" title="http://www.cybyte.com"> http://www.cybyte.com </a></li>
					 <ul>
				</div>
		</div>
		
	</div>
</div>
<?php endif; ?>
