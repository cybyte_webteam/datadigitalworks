<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'datadigitalworks');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@Cd,E-65<yLn6_ZT[/!26-u6x/VIS//#*r#A$AV@C;c{5[NGf]1s`i=JEnnyFPxE');
define('SECURE_AUTH_KEY',  'V.~E|(*FHDOWcLyf3!`Qe*0}RxP$Ax%KgM sMwBKr/DG?RHP*VWsv:DB%$K7tpIB');
define('LOGGED_IN_KEY',    'k`aX=rQo]X_p*K|}DXbibkD7agAx)P@/xW`A|_IUH>}s&31!jZe7(a?m>:<4qV%A');
define('NONCE_KEY',        'F4~rhARZA*e^c7*r<;sjazL/}{}X(txMDzji~UN_`J1a&0RAJd9Z[dS|rUi8{*V+');
define('AUTH_SALT',        'W5Y3y {*2nf?De,JMsYU,=Q@H&>x<jKF;[kUcZ@;> &3uY [,=~`}q_UtfeUJ<a-');
define('SECURE_AUTH_SALT', 'z</[$Hv`J1)mEqL?^ZD (Z<CGN^M;#h7+KC}.i;r<TIi2)u^2{gaBVCepgZ>JgmH');
define('LOGGED_IN_SALT',   'dm89p2<ZDK@_|YU((hWHv3$sS<9h>L HT&niE^{:{g3phGi|B$eXDZnv27RXP9eb');
define('NONCE_SALT',       '{[Hai4.;[Eq|&J|C(4=O0i>NX675![]VX^S%H(]Vn[?|zHChGC?hYh&/E^c`JqIK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
